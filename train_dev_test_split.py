#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
train_dev_test_split.py
Usage:
    train_dev_test_split.py [options]

Options:
    -h --help                               show this screen.
    --dev=<int>                             dev set size [default: 5000]
    --test=<int>                            test set size [default: 5000]
    --data_path=<str>                       path to input file
    --output_dir=<str>                      path to output path to save train.csv/val.csv/test.csv after splitting the data
    --random-state=<int>                    random state for pandas dataframe shuffle [default: None]

"""

import pandas as pd
from docopt import docopt

args = docopt(__doc__)
df_path = str(args['--data_path'])
output_path = str(args['--output_dir'])
print('--data_path: ', df_path)
print('--output_dir: ', output_path)

df_tweets = pd.read_csv(df_path)

"""
df_tweets contains preprocessed Tweets text. See "Preprocessing.py" for details
Stopwords remain.
No lemmatization.
Replicated post-processed Tweets are dropped.

columns: ['label', 'id', 'text', 'ProcessedText', 
          'ProcessedText_length', 'ProcessedText_BERT',
          'ProcessedText_BERTbase_length', 'Mapped_label']
"""

try:
    random_state = int(args['--random-state'])
except:
    random_state = None

# shuffle the data
df_tweets = df_tweets.sample(frac=1, random_state=random_state)
n_val = int(args['--dev'])
n_test = int(args['--test'])
n_train = df_tweets.shape[0] - n_val - n_test
df_train = df_tweets.iloc[:n_train].sort_values(by='ProcessedText_length', ascending=False)
df_val = df_tweets.iloc[n_train:n_train + n_val].sort_values(by='ProcessedText_length', ascending=False)
df_test = df_tweets.iloc[n_train + n_val:].sort_values(by='ProcessedText_length', ascending=False)

df_train.to_csv(str(output_path) + '/train.csv', index=False)
df_val.to_csv(str(output_path) + '/val.csv', index=False)
df_test.to_csv(str(output_path) + '/test.csv', index=False)