# Pre processing the BERT's input algorithm

"""
Preprocessing.py
Usage:
    Preprocessing.py [options]

Options:
    -h --help                               show this screen.
    --data_path=<str>                       path to input file for pre processing
    --output_dir=<str>                      path to output file after pre processing

"""


import re
import pandas as pd
from docopt import docopt
from ekphrasis.classes.preprocessor import TextPreProcessor
from ekphrasis.classes.tokenizer import SocialTokenizer
from ekphrasis.dicts.emoticons import emoticons
from pytorch_pretrained_bert import BertTokenizer


def TweetPreProcessing(df):
    # Insert extra columns into the df with default 0 and None values
    df['ProcessedText'] = None
    df['ProcessedText_length'] = 0
    df['ProcessedText_BERT'] = None
    df['ProcessedText_BERTbase_length'] = 0
    print(df.columns)

    text_processor = TextPreProcessor(  # terms that will be normalized
        normalize=['url', 'email', 'percent', 'money', 'phone', 'user', 'time', 'url', 'date', 'number'],
        # terms that will be annotated
        annotate={"hashtag", "allcaps", "elongated", "repeated", 'emphasis', 'censored'}, fix_html=True,  # fix HTML tokens

        # corpus from which the word statistics are going to be used
        # for word segmentation
        segmenter="twitter",

        # corpus from which the word statistics are going to be used
        # for spell correction
        corrector="twitter",

        unpack_hashtags=True,  # perform word segmentation on hashtags
        unpack_contractions=True,  # Unpack contractions (can't -> can not)
        spell_correct_elong=True,  # spell correction for elongated words

        # select a tokenizer. You can use SocialTokenizer, or pass your own
        # the tokenizer, should take as input a string and return a list of tokens
        tokenizer=SocialTokenizer(lowercase=True).tokenize,

        # list of dictionaries, for replacing tokens extracted from the text,
        # with other expressions. You can pass more than one dictionaries.
        dicts=[emoticons])

    emoji_pattern = re.compile("["
             u"\U0001F600-\U0001F64F"  # emoticons
             u"\U0001F300-\U0001F5FF"  # symbols & pictographs
             u"\U0001F680-\U0001F6FF"  # transport & map symbols
             u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
             u"\U00002702-\U000027B0"
             u"\U000024C2-\U0001F251"
             "]+", flags=re.UNICODE)

    emoji_pattern_1 = re.compile(
        u"(\ud83d[\ude00-\ude4f])|"  # emoticons
        u"(\ud83c[\udf00-\uffff])|"  # symbols & pictographs (1 of 2)
        u"(\ud83d[\u0000-\uddff])|"  # symbols & pictographs (2 of 2)
        u"(\ud83d[\ude80-\udeff])|"  # transport & map symbols
        u"(\ud83c[\udde0-\uddff])"  # flags (iOS)
        "+", flags=re.UNICODE)

    emoji_pattern_2 = re.compile(
        u'([\U00002600-\U000027BF])|'
        u'([\U0001f300-\U0001f64F])|'
        u'([\U0001f680-\U0001f6FF])'
        '+', flags=re.UNICODE)

    emoji_pattern_3 = re.compile(
        u'(\u00a9|\u00ae|[\u2000-\u3300]|'
        u'\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|'
        u'\ud83e[\ud000-\udfff])'
        '+', flags=re.UNICODE)

    # Tweets pre-processing
    for index, row in df.iterrows():
        # remove emojies
        s = df.loc[index, 'text']
        # s = s.encode().decode('unicode-escape')
        s = "".join(emoji_pattern.sub(r' ', s))
        s = "".join(emoji_pattern_1.sub(r' ', s))
        s = "".join(emoji_pattern_2.sub(r' ', s))
        s = "".join(emoji_pattern_3.sub(r' ', s))
        # remove RT and USER
        s = "".join(re.sub('RT @[\w_]+: ', ' ', s))
        # df.loc[index, 'text'] = "".join(re.sub(r'&#  ;', ' ', df.loc[index, 'text']))
        # df.loc[index, 'text'] = "".join(re.sub(r' &# ;', ' ', df.loc[index, 'text']))
        # remove special characters
        s = "".join(re.sub(r'&#\d+;', ' ', s))
        # pre-processing
        s = " ".join(text_processor.pre_process_doc((s)))
        s = "".join(re.sub(r'\<[^>]*\>', ' ', s))
        # Remove non-ascii words or characters
        s = "".join([i if ord(i) < 128 else '' for i in s])
        s = s.replace(r'_[\S]?',r'')
        s = s.replace(r'[ ]{2, }',r' ')
        # Remove &, < and >
        s = s.replace(r'&amp;?', r'and')
        s = s.replace(r'&lt;', r'<')
        s = s.replace(r'&gt;', r'>')
        # Insert space between words and punctuation marks
        s = s.replace(r'([\w\d]+)([^\w\d ]+)', r'\1 \2')
        s = s.replace(r'([^\w\d ]+)([\w\d]+)', r'\1 \2')
        # Calculate text length for later use in LSTM
        s_length = len(s.split())
        # save ProcessedText and ProcessedText_length in final df
        df.loc[index, 'ProcessedText'] = s.strip()
        df.loc[index, 'ProcessedText_length'] = s_length

    # Drop texts with length <=2 and drop duplicates
    df = df[df['ProcessedText_length'] > 2]
    df = df.drop_duplicates(subset=['ProcessedText'])

    # BERT preprocess
    df['ProcessedText_BERT'] = '[CLS] ' + df.ProcessedText
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    df['ProcessedText_BERTbase_length'] = [len(tokenizer.tokenize(sent)) for sent in df.ProcessedText_BERT]
    # tokenizer = BertTokenizer.from_pretrained('bert-large-uncased')
    # df['ProcessedText_BERTlarge_length'] = [len(tokenizer.tokenize(sent)) for sent in df.ProcessedText_BERT]

    label_dict = dict()
    for i, l in enumerate(list(df.label.value_counts().keys())):
        label_dict.update({l: i})

    df['Mapped_label'] = [label_dict[label] for label in df.label]
    return df


if __name__ == '__main__':
    args = docopt(__doc__)
    file_path = str(args['--data_path'])
    output_path = str(args['--output_dir'])
    print('--data_path: ', file_path)
    print('--output_dirL ', output_path)
    df = pd.read_csv(file_path)

    # file_path = './Data/zaraak_final.csv'
    df_processed = TweetPreProcessing(df)
    df_processed.to_csv(str(output_path) + '/tweets_preprocessed.csv', index=False)